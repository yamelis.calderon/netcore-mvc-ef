using Microsoft.EntityFrameworkCore;

namespace Turnos.Models
{
    public class TurnosContext : DbContext 
    {
        public TurnosContext(DbContextOptions<TurnosContext> opciones) : base(opciones)       
        {

        } 

        public DbSet<Especialidad> Especialidad {get; set;}  
        public DbSet<Paciente> Paciente {get;set;}
        protected override void OnModelCreating(ModelBuilder modelBuilder){
            modelBuilder.Entity<Especialidad>(entidad => {
                entidad.ToTable("Especialidad");
                entidad.HasKey(_especialidad => _especialidad.IdEspecialidad);
                entidad.Property(_especialidad => _especialidad.Descripcion)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false);
            });

            modelBuilder.Entity<Paciente>(entidad =>{
                 entidad.ToTable("Paciente");
                entidad.HasKey(_paciente => _paciente.IdPaciente);
                entidad.Property(_paciente => _paciente.Nombre)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

                entidad.Property(_paciente => _paciente.Apellido)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

                entidad.Property(_paciente => _paciente.Direccion)
                .IsRequired()
                .HasMaxLength(250)
                .IsUnicode(false);

                entidad.Property(_paciente => _paciente.Telefono)
                .IsRequired()
                .HasMaxLength(26)
                .IsUnicode(false);

                entidad.Property(_paciente => _paciente.email)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            });
        }
    }
}